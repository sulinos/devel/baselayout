#!/bin/bash
alias clear='echo -ne "\033c" ; clear'
alias hs='history | grep'
mkcd() {
        if [[ $# != 1 ]]; then
                echo "Usage: mkcd <dir>"
        else
                mkdir -p $1 && cd $1
        fi
}
/(){
cd /
}
weather(){
curl "wttr.in/$1"
}
#tweak from https://www.topbug.net/blog/2016/11/28/a-better-ls-command/
if ls --help |& grep -i busybox &>/dev/null ; then
  function ls {
    command ls -F -h --color=always -v -C "$@"
  }
else
  if ls --color -d . >/dev/null 2>&1; then  # GNU ls
    eval "$(dircolors)"
    function ls {
      command ls -F -h --color=always -v --author --time-style=long-iso -C "$@" | less -R -X -F
    }
  fi
fi
alias ll='ls -l'
alias l='ls -l -a'
alias la='ls -a'
#tweak from https://www.topbug.net/blog/2016/09/27/make-gnu-less-more-powerful/
if less --help |& grep -i busybox &>/dev/null ; then
    export LESS='-F -R'
else
    export LESS='-F -i -J -M -R -W -x4 -X -z-4'
fi
# Set colors for less. Borrowed from https://wiki.archlinux.org/index.php/Color_output_in_console#less .
export LESS_TERMCAP_mb=$'\E[1;31m'     # begin bold
export LESS_TERMCAP_md=$'\E[1;36m'     # begin blink
export LESS_TERMCAP_me=$'\E[0m'        # reset bold/blink
export LESS_TERMCAP_so=$'\E[01;44;33m' # begin reverse video
export LESS_TERMCAP_se=$'\E[0m'        # reset reverse video
export LESS_TERMCAP_us=$'\E[1;32m'     # begin underline
export LESS_TERMCAP_ue=$'\E[0m'        # reset underline

alias myip="wget -O - http://ipecho.net/plain; echo"
alias myip2="wget -O - http://ifconfig.me; echo"
alias ~="cd ~"
alias ..='cd ..'
alias ...="cd ../../"
alias ....="cd ../../../"
alias st='git st'
alias gd='git diff'
alias ga='git add'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias debug="set -o nounset; set -o xtrace"
alias fgrep='fgrep --color=auto'
alias bc='bc -l'
alias mkdir='mkdir -pv'
alias path='echo -e ${PATH//:/\\n}'
alias now='date +"%T"'
alias nowtime=now
alias nowdate='date +"%d-%m-%Y"'
alias ports='netstat -tulanp'
alias meminfo='free -m -l -t'
alias psmem='ps auxf | sort -nr -k 4'
alias psmem10='ps auxf | sort -nr -k 4 | head -10'
alias pscpu='ps auxf | sort -nr -k 3'
alias pscpu10='ps auxf | sort -nr -k 3 | head -10'
alias wget='wget -c'
alias cp='cp -p'
alias apt='inary'
alias apt-get='inary'
alias setclip='xclip -selection c'
alias getclip='xclip -selection clipboard -o'
alias pb="curl -sF "c=@${1:--}" -w "%{redirect_url}" 'https://ptpb.pw/?r=1' -o /dev/stderr | setclip"
alias cpb="getclip | pb"
alias paste="curl -F c=@- https://ptpb.pw/"
alias paste2="curl -F 'f:1=<-' ix.io"
alias paste4="curl -F'file=<-' http://0x0.st"
alias paste3="busybox nc termbin.com 9999"
alias serve='python3 -m http.server'
termbin() {
    cat $1 |& busybox nc termbin.com 9999
}
alias gcc32="gcc -m32"
alias g++32="g++ -m32"
alias ga="git add"
alias gc="git commit"
alias gac="git add . && git commit -a -m "
alias gcm="git commit -m"
alias gcl="git clone"
alias gco="git checkout"
alias gds="git diff --staged"
alias gdu="git diff --unstaged"
alias gfa="git fetch --all"
alias gm="git merge"
alias gpsh="git push"
alias gpll="git pull"
alias gs="git status"
alias gu="git pull"
alias emerge="inary em"
#usefull stuff
alias c='clear'
alias e='export'
alias a='alias'
alias t='touch'
alias m='mkdir -pv'
alias r='rm -rfv'
alias x='chmod +x -R'
alias h='history'
alias e='exit'
alias j='jobs -l'
alias s='sync'
alias rm='rm -I --preserve-root'
alias chown='chown --preserve-root'
alias chmod='chmod --preserve-root'
alias chgrp='chgrp --preserve-root'
alias nocolor='sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2})?)?[mGK]//g"'
