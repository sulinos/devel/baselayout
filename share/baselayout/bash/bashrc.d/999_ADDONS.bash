#!/bin/bash
# https://serverfault.com/questions/3743/what-useful-things-can-one-add-to-ones-bashrc#3842

up(){
  local d=""
  limit=$1
  for ((i=1 ; i <= limit ; i++))
    do
      d=$d/..
    done
  d=$(echo $d | sed 's/^\///')
  if [[ -z "$d" ]]; then
    d=..
  fi
  cd $d
}

fawk() {
    first="awk '{print "
    last="}'"
    cmd="${first}\$${1}${last}"
    eval $cmd
}
copyfile() {
    cat "$1" | xclip -selection clipboard
}
grh() {
    grep -rni ./ -e "$@"
}
genpwd() { strings /dev/urandom | grep -o '[[:alnum:]]' | head -n "$1" | tr -d '\n'; echo; }

swap(){ 
    local TMPFILE=tmp.$$

    [[ $# -ne 2 ]] && echo "swap: 2 arguments needed" && return 1
    [[ ! -e $1 ]] && echo "swap: $1 does not exist" && return 1
    [[ ! -e $2 ]] && echo "swap: $2 does not exist" && return 1

    mv "$1" $TMPFILE
    mv "$2" "$1"
    mv $TMPFILE "$2"
}
alias no='yes n'